# Zeppelin Docker with PrestoDB
A zeppelin image that runs in OpenShift and that integrates PrestoDB. the most
important customizations for this are:

1. Adds a patched presto-jdbc jar that allows to add prestodb as interpreter to
  zeppelin via jdbc. 
2. Adds the prestodb interpreter to the interpreters configuration file. 
3. Executes the *fixUID* script that adds group 0 to /etc/passwd so that the
   user used by OpenShift can actually execute different zeppelin commands.
