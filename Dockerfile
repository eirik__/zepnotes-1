FROM apache/zeppelin:0.9.0

USER root
RUN apt-get update
RUN apt-get -y install iputils-ping
RUN apt-get -y install netcat

ENV SPARK_VERSION 2.4.5
ENV HADOOP_PROFILE 2.7
ENV SPARK_HOME /opt/spark

# install spark
# RUN curl -s http://www.apache.org/dist/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop$HADOOP_PROFILE.tgz | tar -xz -C /usr/local/
RUN curl -s http://archive.apache.org/dist/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop$HADOOP_PROFILE.tgz | tar -xz -C /opt/
RUN cd /opt && ln -s spark-$SPARK_VERSION-bin-hadoop$HADOOP_PROFILE spark

RUN chmod -R 777 /opt/zeppelin && chown -R 1000360000:1000360000 /opt/zeppelin
RUN chmod -R 777 /opt/spark-$SPARK_VERSION-bin-hadoop$HADOOP_PROFILE && chown -R 1000360000:1000360000 /opt/spark-$SPARK_VERSION-bin-hadoop$HADOOP_PROFILE

ADD presto-jdbc-0.216.jar /opt/zeppelin/interpreter/jdbc
ADD presto-jdbc-0.216.jar /opt/zeppelin/interpreter/spark/dep
# ADD interpreter.json /opt/zeppelin/conf
ADD zeppelin-env.sh /opt/zeppelin/conf
ADD zeppelin-site.xml /opt/zeppelin/conf
ADD log4j.properties /opt/zeppelin/conf
# ADD interpreter.sh /opt/zeppelin/bin
ADD fixUID.sh /opt/zeppelin

# RUN chmod 777 /opt/zeppelivn/bin/interpreter.sh
# RUN chmod 777 /opt/zeppelin/conf/interpreter.json
RUN chmod 777 /opt/zeppelin/conf/zeppelin-env.sh
RUN chmod 777 /opt/zeppelin/conf/zeppelin-site.xml
RUN chmod 777 /opt/zeppelin/conf/log4j.properties
RUN chmod 755 /opt/zeppelin/fixUID.sh

EXPOSE 8080
EXPOSE 4040
EXPOSE 4041
EXPOSE 7078
EXPOSE 7079

RUN chgrp root /etc/passwd && chmod ug+rw /etc/passwd

USER 1000360000

CMD ./fixUID.sh && ./bin/zeppelin.sh
